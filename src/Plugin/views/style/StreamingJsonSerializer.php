<?php

namespace Drupal\views_streaming_data\Plugin\views\style;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "json_streaming_data",
 *   title = @Translation("JSON streaming Serializer"),
 *   help = @Translation("Serializes views row data to JSON."),
 *   display_types = {"streaming_data"}
 * )
 */
class StreamingJsonSerializer extends StylePluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;


  /**
   * The display object this plugin is for.
   *
   * @var \Drupal\views_streaming_data\Plugin\views\display\StreamingDisplayInterface
   */
  public $displayHandler;

  /**
   * Overrides \Drupal\views\Plugin\views\PluginBase::init().
   *
   * The style options might come externally as the style can be sourced from at
   * least two locations. If it's not included, look on the display.
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->displayHandler->setMimeType('application/json');
    $this->displayHandler->setFileExtension('json');
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $stream = $this->displayHandler->getOutputStream();
    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    $headers = NULL;

    // We expect to be able to traverse the view, but sanity check here.
    if ($this->view instanceof \Traversable) {
      $traversable = $this->view;
    }
    else {
      $traversable = $this->view->result;
    }
    // Simple hack here to write out manually the opening and closing brackets
    // representing a JSON array that wraps the rows.
    fwrite($stream, '[');
    $before = "\n";
    foreach ($traversable as $row_index => $row) {
      $data = $this->view->rowPlugin->render($row);
      fwrite($stream, $before . json_encode($data));
      // Add a comma before each row after the first row.
      $before = ",\n";
    }
    fwrite($stream, "\n]");
    return '';
  }

  /**
   * Gets the available format that can be requested.
   *
   * @return string
   *   An format.
   */
  public function getFormat() {
    return 'json';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['request_format'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
