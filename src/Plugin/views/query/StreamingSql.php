<?php

namespace Drupal\views_streaming_data\Plugin\views\query;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\views_streaming_data\StreamingViewExecutable;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\query\DateSqlInterface;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\query\Sql;

/**
 * Views query plugin for an SQL query.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "streaming_sql_query",
 *   title = @Translation("Streaming SQL Query"),
 *   help = @Translation("Query will be generated and run using the Drupal database API.")
 * )
 */
class StreamingSql extends Sql {

  /**
   * The SQL query result.
   *
   * @var \Drupal\Core\Database\Statement
   */
  public $result;

  /**
   * The entity memory cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $entityMemoryCache;

  /**
   * Constructs a Sql object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\views\Plugin\views\query\DateSqlInterface $date_sql
   *   The database-specific date handler.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Cache\CacheBackendInterface $entity_cache
   *   The entity memory cache.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, DateSqlInterface $date_sql, MessengerInterface $messenger, CacheBackendInterface $entity_cache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $date_sql, $messenger);

    $this->entityMemoryCache = $entity_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('views.date_sql'),
      $container->get('messenger'),
      $container->get('entity.memory_cache')
    );
  }

  /**
   * Executes the query and saves the result for later iteration.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->current_page.
   */
  public function execute(ViewExecutable $view) {
    // Only run the special code here if all the starts are aligned correctly.
    // Skip all code related to count queries and pagers.
    if ($view instanceof StreamingViewExecutable) {
      $query = $view->build_info['query'];

      $query->addMetaData('view', $view);

      if (empty($this->options['disable_sql_rewrite'])) {
        $base_table_data = Views::viewsData()->get($this->view->storage->get('base_table'));
        if (isset($base_table_data['table']['base']['access query tag'])) {
          $access_tag = $base_table_data['table']['base']['access query tag'];
          $query->addTag($access_tag);
        }

        if (isset($base_table_data['table']['base']['query metadata'])) {
          foreach ($base_table_data['table']['base']['query metadata'] as $key => $value) {
            $query->addMetaData($key, $value);
          }
        }
      }
      $start = microtime(TRUE);
      if ($query) {
        try {
          /** @var \Drupal\Core\Database\Statement $result */
          $result = $query->execute();
          $result->setFetchMode(\PDO::FETCH_CLASS, ResultRow::class);
          $this->result = $result;

          // Setup the result row objects. This is an empty array to avoid
          // any unexpected side effects. The view will iterate over the
          // SQL query result in StreamingViewExecutable::getIterator().
          $view->result = [];

          $view->total_rows = NULL;

        }
        catch (DatabaseExceptionWrapper $e) {
          $view->result = [];
          if (!empty($view->live_preview)) {
            $this->messenger->addError($e->getMessage());
          }
          else {
            throw new DatabaseExceptionWrapper("Exception in {$view->storage->label()}[{$view->storage->id()}]: {$e->getMessage()}");
          }
        }

      }
      $view->execute_time = microtime(TRUE) - $start;
    }
    else {
      parent::execute($view);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntities(&$results) {
    parent::loadEntities($results);
    // Clear entity static cache to avoid OOM.
    $this->entityMemoryCache->deleteAll();
  }

}
