<?php

namespace Drupal\views_streaming_data\Plugin\views\display;

use Drupal\views\Plugin\views\display\ResponseDisplayPluginInterface;

/**
 * Interface StreamingDisplayInterface.
 */
interface StreamingDisplayInterface extends ResponseDisplayPluginInterface {

  const DEFAULT_CHUNK_SIZE = 50;

  /**
   * Set the preferred output stream.
   *
   * @param resource $stream
   *   A stream resource (file pointer) open for writing.
   */
  public function setOutputStream($stream);

  /**
   * Get the preferred output stream.
   *
   * @return resource
   *   A stream resource (file pointer) open for writing.
   */
  public function getOutputStream();

  /**
   * Sets the request content type.
   *
   * @param string $mime_type
   *   The response mime type. E.g. 'application/json'.
   */
  public function setMimeType($mime_type);

  /**
   * Gets the mime type.
   *
   * This will return any overridden mime type, otherwise returns the mime type
   * from the request.
   *
   * @return string
   *   The response mime type. E.g. 'application/json'.
   */
  public function getMimeType();

  /**
   * Sets the file extension.
   *
   * @param string $extension
   *   The file extension E.g. 'txt'.
   */
  public function setFileExtension($extension);

  /**
   * Gets the content type.
   *
   * @return string
   *   The file extension. E.g. 'txt'.
   */
  public function getFileExtension();

  /**
   * Gets the file name to be used in the response header.
   *
   * @return string
   *   The file name. E.g. 'foo.csv'.
   */
  public function getExportFileName();

}
