<?php

namespace Drupal\views_streaming_data;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\ViewsData;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the view factory.
 */
class ViewExecutableFactory {

  /**
   * Stores the current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The views data.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * View entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Constructs a new ViewExecutableFactory.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\views\ViewsData $views_data
   *   The views data.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(AccountInterface $user, RequestStack $request_stack, ViewsData $views_data, RouteProviderInterface $route_provider, EntityTypeManagerInterface $entity_type_manager) {
    $this->user = $user;
    $this->requestStack = $request_stack;
    $this->viewsData = $views_data;
    $this->routeProvider = $route_provider;
    $this->entityStorage = $entity_type_manager->getStorage('view');
  }

  /**
   * Instantiates a StreamingViewExecutable class.
   *
   * @param string $view_id
   *   A view entity ID.
   *
   * @return \Drupal\views_streaming_data\StreamingViewExecutable|null
   *   A StreamingViewExecutable instance.
   */
  public function get($view_id) {
    /** @var \Drupal\views\ViewEntityInterface $view_entity */
    $view_entity = $this->entityStorage->load($view_id);
    $view = NULL;
    if ($view_entity) {
      $view = new StreamingViewExecutable($view_entity, $this->user, $this->viewsData, $this->routeProvider);
      $view->setRequest($this->requestStack->getCurrentRequest());
    }
    return $view;
  }

}
