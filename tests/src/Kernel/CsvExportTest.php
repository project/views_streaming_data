<?php

namespace Drupal\Tests\views_streaming_data\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\views\Views;

/**
 * Tests CSV export compared to csv_serialization module.
 *
 * @group views_streaming_data
 */
class CsvExportTest extends EntityKernelTestBase {

  use ExportTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'views',
    'rest',
    'serialization',
    'csv_serialization',
    'views_streaming_data',
    'views_streaming_data_test',
    'options',
  ];

  /**
   * Initial test compare to csv_serialization.
   */
  public function testCsv1() {
    $stream = fopen('php://memory', 'r+b');
    $view_id = 'test_basic_content_export';

    // It would be a better match to render the array returned by
    // RestExport::buildBasicRenderable(), but that's not working for
    // some reason.
    $view = Views::getView($view_id);
    $view->setDisplay('rest_export_csv_1');
    $display = $view->getDisplay();
    // Likely not be needed, but assure we are getting expected type.
    $display->setContentType('csv');
    $build = $view->executeDisplay();
    // Strip trailing CRs that may differ.
    $output = rtrim((string) $build['#markup'], "\n");
    $this->assertNotEmpty($output);
    $this->assertCount(count($this->nodes), $view->result);

    /** @var \Drupal\views_streaming_data\StreamingViewExecutable $streaming_view */
    $streaming_view = \Drupal::service('views_streaming_data.views.executable')->get($view_id);
    $streaming_view->setDisplay('streaming_data_export_csv_1');
    /** @var \Drupal\views_streaming_data\Plugin\views\display\StreamingDataExport $display */
    $display = $streaming_view->getDisplay();
    $display->setOutputStream($stream);
    // By default the strip_tags and trim options should be true even if not
    // in the YAML file.
    $style = $streaming_view->getStyle();
    $this->assertTrue($style->options['strip_tags'], 'strip_tags option');
    $this->assertTrue($style->options['trim'], 'trim option');
    $streaming_view->executeDisplay();
    rewind($stream);
    $streamed_output = rtrim(stream_get_contents($stream), "\n");
    $this->assertNotEmpty($streamed_output);

    $this->assertEquals($output, $streamed_output);
    // Tags stripped.
    $this->assertStringContainsString(",angry", $streamed_output);
    // Whitespace trimmed.
    $this->assertStringContainsString('"node 6",', $streamed_output);
  }

  /**
   * Initial test compare to csv_serialization without strip_tags.
   */
  public function testCsv2() {
    $stream = fopen('php://memory', 'r+b');
    $view_id = 'test_basic_content_export';

    // It would be a better match to render the array returned by
    // RestExport::buildBasicRenderable(), but that's not working for
    // some reason.
    $view = Views::getView($view_id);
    $view->setDisplay('rest_export_csv_1');
    $display = $view->getDisplay();
    $style = $view->getStyle();
    $style->options['csv_settings'] = [
      'strip_tags' => FALSE,
      'trim' => FALSE,
      'delimiter' => ',',
      'enclosure' => '"',
      'escape_char' => "\\",
      'encoding' => 'utf8',
    ];
    // Likely not be needed, but assure we are getting expected type.
    $display->setContentType('csv');
    $build = $view->executeDisplay();
    // Strip trailing CRs that may differ.
    $output = rtrim((string) $build['#markup'], "\n");
    $this->assertNotEmpty($output);
    $this->assertCount(count($this->nodes), $view->result);

    /** @var \Drupal\views_streaming_data\StreamingViewExecutable $streaming_view */
    $streaming_view = \Drupal::service('views_streaming_data.views.executable')->get($view_id);
    $streaming_view->setDisplay('streaming_data_export_csv_2');
    /** @var \Drupal\views_streaming_data\Plugin\views\display\StreamingDataExport $display */
    $display = $streaming_view->getDisplay();
    $display->setOutputStream($stream);
    // The strip_tags and trim options should be false for this display.
    $style = $streaming_view->getStyle();
    $this->assertFalse($style->options['strip_tags'], 'strip_tags option');
    $this->assertFalse($style->options['trim'], 'trim option');
    $streaming_view->executeDisplay();
    rewind($stream);
    $streamed_output = rtrim(stream_get_contents($stream), "\n");
    $this->assertNotEmpty($streamed_output);

    $this->assertEquals($output, $streamed_output);
    // Tags not stripped.
    $this->assertStringContainsString("<em>angry</em>", $streamed_output);
    // Whitespace not trimmed.
    $this->assertStringContainsString("\"\t node 6\",", $streamed_output);
  }

  /**
   * Initial test of pipe delimited output.
   */
  public function testPipe() {
    $stream = fopen('php://memory', 'r+b');
    $view_id = 'test_basic_content_export';

    /** @var \Drupal\views_streaming_data\StreamingViewExecutable $streaming_view */
    $streaming_view = \Drupal::service('views_streaming_data.views.executable')->get($view_id);
    $streaming_view->setDisplay('streaming_data_export_csv_1');
    /** @var \Drupal\views_streaming_data\Plugin\views\display\StreamingDataExport $display */
    $display = $streaming_view->getDisplay();
    $display->setOutputStream($stream);
    $style = $streaming_view->getStyle();
    $style->options['delimiter'] = '|';
    $streaming_view->executeDisplay();
    rewind($stream);
    $header = fgets($stream);
    $this->assertNotEmpty($header);
    // Pipe in the header should be replaced.
    $this->assertStringContainsString('Some ; Codes', $header);
    $rows = [];
    while ($s = fgets($stream)) {
      $rows[] = $s;
    }
    $this->assertCount(count($this->nodes), $rows);
  }

}
