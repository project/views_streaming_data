<?php

namespace Drupal\Tests\views_streaming_data\Kernel;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Shared test code.
 */
trait ExportTestTrait {

  /**
   * The content we created.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $nodes = [];

  /**
   * Setup for the test.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'text', 'filter']);
    $this->installConfig(self::$modules);

    $this->installSchema('node', ['node_access']);
    $this->createUser(['uid' => 1]);
    // Quick trick to allow the format to be used.
    $config = $this->container->get('config.factory')->getEditable('filter.settings');
    $config->set('fallback_format', 'restricted_html')->save();
    $this->createContent();
  }

  /**
   * Create sample content.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createContent() {
    $content = [
      [
        'title' => 'node 1',
        'field_short_description' => 'Zow',
        'field_some_codes' => [0],
        'created' => 1555554321,
      ],
      [
        'title' => 'node 2',
        'field_short_description' => '<p>hi</p>',
        'field_some_codes' => [1, 2],
        'created' => 1555654321,
      ],
      [
        'title' => 'node 3',
        'field_short_description' => 'hi,i, j',
        'field_some_codes' => [3],
        'created' => 1555554322,
      ],
      [
        'title' => 'node 4',
        'field_short_description' => '<em>angry</em>',
        'field_some_codes' => [4],
        'created' => 1555554323,
      ],
      [
        'title' => 'node 5',
        'field_short_description' => 'Happy!',
        'field_some_codes' => [0, 3, 5],
        'created' => 1555544321,
      ],
      [
        'title' => "\t node 6",
        'field_short_description' => 'blah',
        'field_some_codes' => [2],
        'created' => 1555554321,
      ],
    ];
    foreach ($content as $params) {
      $this->nodes[] = $this->createBasicContent($params);
    }
  }

  /**
   * Helper function to create a node.
   *
   * @param array $params
   *   The node properties.
   *
   * @return \Drupal\node\NodeInterface
   *   The created, validated, and saved node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createBasicContent(array $params):NodeInterface {
    $params += [
      'uid' => '1',
      'langcode' => 'en',
      'status' => 1,
      'type' => 'basic_content',
    ];
    if (isset($params['field_short_description']) && is_string($params['field_short_description'])) {
      $value = $params['field_short_description'];
      $params['field_short_description'] = [
        'format' => 'restricted_html',
        'value' => $value,
      ];
    }
    $node = Node::create($params);
    $errors = $node->validate();
    $messages = [];
    foreach ($errors as $e) {
      $messages[] = (string) $e;
    }
    $this->assertCount(0, $errors, implode("\n", $messages));
    $node->save();
    return $node;
  }

}
