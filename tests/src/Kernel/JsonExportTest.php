<?php

namespace Drupal\Tests\views_streaming_data\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\views\Views;

/**
 * Tests JSON export.
 *
 * @group views_streaming_data
 */
class JsonExportTest extends EntityKernelTestBase {

  use ExportTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'views',
    'rest',
    'serialization',
    'csv_serialization',
    'views_streaming_data',
    'views_streaming_data_test',
    'options',
  ];

  /**
   * Initial test compare to core json serialization.
   */
  public function testJson() {
    $stream = fopen('php://memory', 'r+b');
    $view_id = 'test_basic_content_export';

    // It would be a better match to render the array returned by
    // RestExport::buildBasicRenderable(), but that's not working for
    // some reason.
    $view = Views::getView($view_id);
    $view->setDisplay('rest_export_json');
    $display = $view->getDisplay();
    // Likely not be needed, but assure we are getting expected type.
    $display->setContentType('json');
    $build = $view->executeDisplay();
    $output = (string) $build['#markup'];
    $this->assertNotEmpty($output);
    $this->assertCount(count($this->nodes), $view->result);

    /** @var \Drupal\views_streaming_data\StreamingViewExecutable $streaming_view */
    $streaming_view = \Drupal::service('views_streaming_data.views.executable')->get($view_id);
    $streaming_view->setDisplay('streaming_data_json');
    /** @var \Drupal\views_streaming_data\Plugin\views\display\StreamingDataExport $display */
    $display = $streaming_view->getDisplay();
    $display->setOutputStream($stream);
    $streaming_view->executeDisplay();
    rewind($stream);
    $streamed_output = stream_get_contents($stream);
    $this->assertNotEmpty($streamed_output);

    $core = json_decode($output);
    $streamed = json_decode($streamed_output);
    $this->assertEquals($core, $streamed);
    $this->assertCount(count($this->nodes), $streamed);
  }

}
