Views Streaming Data

This module provides a views display type for CSV and JSON for exporting large
result sets using a streaming Response and limiting the amount of memory
consumed by the entity system. The goal is to allow exporting 100k and
larger result sets without running out of PHP memory.
